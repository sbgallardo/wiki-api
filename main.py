from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse, RedirectResponse
import requests

app = FastAPI()

origins = ['*']

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_methods=["*"],
    allow_headers=["*"],
)

server_url = 'http://localhost:9090'
wiki_api_url = 'https://es.wikipedia.org/w/api.php?action=query&format=json&prop=revisions&rvprop=content&titles='


def fetch_article(art_name: str):
    query = wiki_api_url + art_name
    response = requests.get(query)
    return response.json()


@app.get("/")
async def root():
    return RedirectResponse(url="/docs")


@app.get("/a/{art_name}")
@app.get("/article/{art_name}")
async def get_article(art_name: str):
    article = fetch_article(art_name)
    return JSONResponse(content=article, status_code=200)


if __name__ == "__main__":
    import uvicorn

    app_name = "main:app"
    uvicorn.run(app=app_name, host="0.0.0.0", port=4040, reload=False)
